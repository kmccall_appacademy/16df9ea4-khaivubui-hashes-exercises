# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_lengths = Hash.new(0)
  str.split.each do |word|
    word_lengths[word] = word.length
  end
  word_lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |_, v| v }.last.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  updated_older_inventory = older
  newer.each do |k, v|
    updated_older_inventory[k] = v
  end
  updated_older_inventory
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_counter = Hash.new(0)
  word.chars.each do |ch|
    letter_counter[ch] += 1
  end
  letter_counter
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = Hash.new(0)
  arr.each do |el|
    hash[el] = true
  end
  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_vs_odd_counter = Hash.new(0)
  numbers.each do |num|
    even_vs_odd_counter[:even] += 1 if num.even?
    even_vs_odd_counter[:odd] += 1 if num.odd?
  end
  even_vs_odd_counter
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = 'aeiou'
  vowel_counts = Hash.new(0)
  string.chars.each do |ch|
    vowel_counts[ch] += 1 if vowels.include?ch
  end
  highest_vowel_count = vowel_counts.sort_by { |_, v| v }.last.last
  vowel_counts.select { |_, v| v == highest_vowel_count }.sort_by do |k, _|
    k
  end.first.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  students.select! { |_, birthmonth| birthmonth > 6 }
  combination_array = []
  second_half_students = students.keys
  second_half_students.each_with_index do |student, index|
    (index + 1..second_half_students.length - 1).each do |i2|
      combination_array << [student, second_half_students[i2]]
    end
  end
  combination_array
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  counter_by_species = Hash.new(0)
  specimens.each do |specimen|
    counter_by_species[specimen] += 1
  end
  number_of_species = counter_by_species.count
  smallest_population_size = counter_by_species.sort_by { |_, v| v }.first.last
  largest_population_size = counter_by_species.sort_by { |_, v| v }.last.last
  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  character_count(normal_sign).each do |ch, ch_count|
    return false if ch_count < character_count(vandalized_sign)[ch]
  end
  true
end

def character_count(str)
  ch_counter = Hash.new(0)
  str.chars.each do |ch|
    ch_counter[ch] += 1
  end
  ch_counter
end
